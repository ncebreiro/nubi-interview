from models.connection import MongoAPI

class AnswersView(object):

    def post(answers):
        res = MongoAPI("answers").write(answers)
        return res

class PollsView(object):

    def post(polls):
        res = MongoAPI("polls").write(data)
        return res
    
    def get():
        polls = MongoAPI("polls").read()
        answers = MongoAPI("answers").read()
        for poll in polls:
            poll['answers'] = [answer for answer in answers if answer['poll_id'] == poll['_id']]
        return polls