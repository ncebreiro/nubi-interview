
from utils.helpers import Singleton
from pymongo import MongoClient

@Singleton
class DBConnection(object):

    def __init__(self):
        self.client = MongoClient(host='db')

    def __str__(self):
        return self.client


class MongoAPI:
    def __init__(self, document):
        client = DBConnection.Instance()
        cursor = client["polls"]
        self.collection = cursor[document]

    def read(self):
        documents =  self.collection.find()
        output = [{item: str(data[item]) for item in data} for data in documents]
        return output

    def find_byid(self,id):
        return self.collection.find_one({"_id" : ObjectId(id)})

    def write(self, data):
        new_document = data
        new_document["CreatedDate"] = datetime.datetime.today()
        result = self.collection.insert_one(new_document)
        return str(result.inserted_id)