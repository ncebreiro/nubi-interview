
from flask import Flask, request, json, Response
from views.views import PollsView, AnswersView

app = Flask(__name__)   

@app.route('/')
def base():
    return "status:up"

@app.route('/addAnswer')
def answers_post():
    data = request.json
    res = AnswersView.post(data)
    return Response(response=json.dumps(res),status=200,mimetype='application/json')

@app.route('/addPoll')
def polls_post():
    data = request.json
    res = PollsView.post(data)
    return Response(response=json.dumps(res),status=200,mimetype='application/json')    

@app.route('/getPolls')
def polls_get():
    polls = PollsView.get()
    return Response(response=json.dumps(polls))
